const SHA256 = require('crypto-js/sha256'); //Constant call SHA256 - Using the Crpyto Lib

class Block {
    constructor(index, timestamp, data, previousHash = '') {
        this.index = index;
        this.timestamp = timestamp;
        this.data = data;
        this.previousHash = previousHash;
        this.hash = '';
    }

    calculateHash() { //calculate a hash and stringify
        return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data)).toString();
    }
}




class Blockchain {
    constructor() {
        this.chain = [this.createGenesisBlock()];
    }
    createGenesisBlock() {
        return new Block(0, "30/07/2018", "Genesis block", "0");
    }
    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }
    addBlock(newBlock) {
        newBlock.previousHash = this.getLatestBlock().hash;
        newBlock.hash = newBlock.calculateHash();
        this.chain.push(newBlock);
    }
    isChainValid() { //Valide integrity of blockchain
        for (let i = 1; i < this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if (currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }

            if (currentBlock.previousHash !== previousBlock.hash) {
                return false;
            }
        }
        return true;
    }
}

let CharlieCoin = new Blockchain();
CharlieCoin.addBlock(new Block(1, "10/07/2017", { amount: 4 }));
CharlieCoin.addBlock(new Block(2, "10/07/2017", { amount: 4 }));


console.log(JSON.stringify(CharlieCoin, null, 4));
//console.log(JSON.stringify(CharlieCoin, null, 4));
